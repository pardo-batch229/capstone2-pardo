const productControllers = require("../controllers/productControllers")
const express = require("express")
const router = express.Router()
const auth = require("../authentication.js")



// Create products
router.post("/create", auth.verify, (req, res)=>{
    
   const data = {
        product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
   }
   productControllers.addProduct(data).then(resultFromController => res.send(resultFromController))
})

// Retrieve active products
router.get("/allProducts", (req, res)=>{
    productControllers.getAllProducts().then(resultFromController => res.send(resultFromController))
})

// Retrieve active products
router.get("/ProductsList", auth.verify, (req, res)=>{

    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    } 

    productControllers.getProductsList(data).then(resultFromController => res.send(resultFromController))
})

// Retrieve single products
router.get("/:productId", (req, res)=>{
    productControllers.getSingleProduct(req.params).then(resultFromController => res.send(resultFromController))
})

// Update Product information
router.put("/updateProduct/:productId", auth.verify, (req, res)=>{
    const data ={
        product: req.body,
        reqParams: req.params.productId,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    productControllers.updateProduct(data).then(resultFromController => res.send(resultFromController))
})

// Archive Product (Admin)
router.put("/:productId/archive", auth.verify, (req,res)=>{

    const data ={
        product: req.body,
        reqParams: req.params.productId,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    productControllers.archiveProduct(data).then(resultFromController => res.send(resultFromController))
})

module.exports = router;

