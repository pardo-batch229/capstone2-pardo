const express = require("express")
const router = express.Router()
const auth = require("../authentication.js")
const userControllers = require("../controllers/userControllers.js")
const orderControllers = require("../controllers/orderControllers.js")
const Products = require("../models/Products")

// User Registration route
router.post("/registerUser", (req, res)=>{
    userControllers.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})
// User Login
router.post("/login", (req, res)=>{
    userControllers.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

// Retrieving user details
router.get("/userDetails", auth.verify, (req, res)=>{
    const data = {
        userData: auth.decode(req.headers.authorization).id
    } 
    userControllers.getProfile(data).then(resultFromController => res.send(resultFromController))
})
// Retrieving all user details
router.get("/usersCredentials", auth.verify, (req, res)=>{
    const data = {
        userData: auth.decode(req.headers.authorization).id,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    userControllers.getAllDetails(data).then(resultFromController => res.send(resultFromController)) 
})

// Set an admin
router.put("/setAdmin/:id", auth.verify, (req, res)=>{
    const data = {
       userData: req.body,
       isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    userControllers.setAnAdmin(req.params, data).then(resultFromController => res.send(resultFromController))
})

//Change Password
router.put("/changePassword/:id", auth.verify, (req, res)=>{
    const data ={
        userData: req.body,
        reqParams: req.params.id,
        userId: auth.decode(req.headers.authorization).id
    }
    userControllers.changePass(data).then(resultFromController => res.send(resultFromController))
})
/* ======================================================================================= */

//Order's checkout
router.post("/checkOut",auth.verify, (req, res)=>{

    const data={
        userId: auth.decode(req.headers.authorization).id,
        userName: auth.decode(req.headers.authorization).userName,
        email: auth.decode(req.headers.authorization).email,
    }

    orderControllers.orderProducts(data, req.body.products).then(resultFromController => res.send(resultFromController))
})

// Get All orders (Admin)
router.get("/allOrders", auth.verify, (req, res)=>{
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    orderControllers.getOrders(data).then(resultFromController => res.send(resultFromController))
})

// Get User's Order
router.get("/myOrder", auth.verify, (req, res)=>{
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        userId: auth.decode(req.headers.authorization).id
    } 
    orderControllers.getMyOrders(data).then(resultFromController => res.send(resultFromController))
})
// to connect the userRoutes to ther modules
module.exports = router