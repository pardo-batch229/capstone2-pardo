const jwtoken = require("jsonwebtoken")

// Token Creation

const secret = "EcomAPI";

module.exports.createAccessToken = (user) =>{
    const data = {
        id: user._id,
        userName: user.userName,
        email: user.email,
        isAdmin: user.isAdmin
    }
    // returns the token in which it holds the data 
    return jwtoken.sign(data, secret, {})
}

//Token Verification
module.exports.verify = (req, res, next) =>{
    // token is retrieved from the request header
    let token = req.headers.authorization;

    //Proceeds if the token is not tampered/undefined
    if(typeof token !== "undefined"){
        // Used slice to remove the Bearer word in token
         token = token.slice(7, token.length)

         return jwtoken.verify(token, secret, (err, data)=>{
            //checks if the token is valid or not valid 
            return err ? res.send({auth: "failed"}) : next();
         })
    }else{
        return res.send({auth: "failed"})
    }
}

// Token decryption
module.exports.decode = (token) =>{

    if(token !== "undefined"){
        token = token.slice(7, token.length)

        return jwtoken.verify(token, secret, (err, data) => {

            if(err){
                return null
            }else{
                return jwtoken.decode(token,{complete:true}).payload;
            }
        })
    }else{
        return null
    }
}

