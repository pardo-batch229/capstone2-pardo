const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    userName : {
        type: String,
        required: [true, "User Name is required!"]
    },
    password:{
        type: String,
        required: [true, "Password is required!"]
    },
    mobileNumber: {
        type: String,
        required: [true, "Mobile Number is required!"]
    },
    firstName: {
        type: String,
        required: [true, "First Name is required!"]
    },
    middleName:{
        type: String,
        default: null
    },
    lastName: {
        type: String,
        required: [true, "Last Name is required!"]
    },
    email: {
        type: String,
        required: [true, "Email is required!"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    }

})


// to export a model into controllers and routes
module.exports = mongoose.model("Users", userSchema)