const mongoose = require("mongoose")


const orderSchema = new mongoose.Schema({
    userId: {
       type: String,
       required: true,
    },
    userName: {
       type: String,
       required: true,
    },
    email: {
       type: String,
       required: true,
    },
    products: [
       {
          productId: {
             type: String,
             required: true,
          },
          productName: {
             type: String
          },
          quantity: {
             type: Number,
             required: true,
          },
          price: {
             type: Number
          }
       },
    ],
    totalAmount: {
       type: Number
    },
    purchasedOn: {
       type: String,
       default: new Date(),
    },
    isActive: {
       type: String,
       default: true,
    },
 });
 
module.exports = mongoose.model("Orders", orderSchema)
