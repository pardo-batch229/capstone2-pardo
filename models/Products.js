const mongoose = require('mongoose');


const productSchema = new mongoose.Schema({
    name:{
        type: String,
        required: [true, "Name of Product is required!"]
    },
    brand:{
        type: String,
        required: [true, "Brand Name is required!"]
    },
    description:{
        type: String,
        required: [true, "Description is required!"]
    },
    price:{
        type: Number,
        required: [true, "Price is required!"]
    },
    stocks:{
        type: Number,
        required: [true, "Stocks required"]
    },
    isActive:{
        type: Boolean,
        default: true
    },
    imgSource:{
        type: String
    },
    createdOn:{
        type: Date,
        default: new Date()
    }
})


module.exports = mongoose.model("Products", productSchema)