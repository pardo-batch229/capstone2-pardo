const Orders = require("../models/Orders.js")
const Products = require("../models/Products.js")
const Users = require("../models/Users.js")


// Place an Order
module.exports.orderProducts = async (data, products) => {
    try {

        
      let isAdmin = await Users.find({
        $and: [
          { isAdmin: true },
          { _id: data.userId }
        ]
      });
  
      if (isAdmin.length > 0) {
        return false;
      } else {
        let updatedOrders = new Orders({
            products: [],
            totalAmount: 0,
            ...data
          });

        let orderSaved = false;
        for (const product of products) {
          let arrayProduct = await Products.findById(product.productId);
          if (!arrayProduct) {
            return false;
          }
          updatedOrders.products.push({
            productId: product.productId,
            quantity: product.quantity,
            productName: arrayProduct.name,
            price: arrayProduct.price,
          });
          updatedOrders.totalAmount += arrayProduct.price * product.quantity;
          orderSaved = true;
        }
        if (orderSaved) {
            await updatedOrders.save();
            return updatedOrders;
          }
      }
    } catch (error) {
      return error;
    }
  };
  

//Get All orders
module.exports.getOrders = async (data) =>{

try{

    if(data.isAdmin){
        return await Orders.find({}).then(result=>{
            return result
        })
    }else{
        return false
    }

}catch(error){
    return error
}
}
// Get User orders

module.exports.getMyOrders = async (data) =>{
  
    if(data.isAdmin){
        return false
    }else{
        return await Orders.find({userId: data.userId}).then(result=>{
            return result
        })

    }
        
        // // if users is Admin
        // if(isAdmin.length > 0){
        //     return {message: "Unauthorized Access"}
        // }else{
        //     return true;
        // }
}
