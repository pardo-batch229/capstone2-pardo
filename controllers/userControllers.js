const bcrypt = require("bcrypt")
const auth = require("../authentication.js")
const Users = require("../models/Users.js")

// User Registration
module.exports.registerUser = async (reqBody)=>{
    return await Users.find({userName: reqBody.userName}).then(result =>{
        console.log(`${result.length}`)
        if(result.length > 0){
            return false
        }else{
            // proceed to register the users
            let newUser = new Users({
                userName: reqBody.userName,
                firstName: reqBody.firstName,
                lastName: reqBody.lastName,
                email: reqBody.email,
                mobileNumber: reqBody.mobileNumber,
                password: bcrypt.hashSync(reqBody.password, 10)
            })
        
            return newUser.save().then((user, error)=>{
                if(error){
                    return false
                }else{
                    return true
                }
            })
        }
    })
    
}
// User authentication
module.exports.loginUser = (reqBody) => {
    return Users.findOne({userName: reqBody.userName}).then(result => {
        if(result == null){
            return false
        }else{
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

            if(isPasswordCorrect){
                return {access: auth.createAccessToken(result)}
            }else{
                return false
            }
        }
    })
}
// User Details
module.exports.getProfile = async (data) =>{
    
    const user = await Users.findById(data.userData);
    // Find user if its match the credetials of his login details then display its credentials
    
    if(data.userData){
        user.password = "";
        return user;
    }else{
        return false
    }
}
//Retrieval of Information of users
module.exports.getAllDetails = async (data) =>{
    
    const user = await Users.find({})
    user.password = ""
    if(data.isAdmin){
        return user
    }else{
        return false
    }
}
// Set Admin
module.exports.setAnAdmin = async (reqParams, data)=>{

try{
    if(data.isAdmin){
        const user = await Users.findById(reqParams.id);
        // if not user found in database
        if (!user) {
            return false
            // set admin
        }else{
            user.isAdmin = data.userData.isAdmin;
            // Save the updated user
            await user.save();
            return true
        }       
    }else{
        return false
    }
}catch(error){
    console.log(error)
    return res.status(500).send({error: "Internal Error"})
}
}

// User change pass
module.exports.changePass = async (data) =>{
    
    // let user = await Users.findById(data.userId)

        return await Users.findById(data.userId).then(async user =>{
            // console.log(user)
            if(data.userId == data.reqParams){
                 const isMatch = await bcrypt.compare(data.userData.currentPassword, user.password)
                  
                 if(isMatch){
                    const salt = await bcrypt.genSalt(10)

                    let updatedPassword = {
                        password: await bcrypt.hash(data.userData.newPassword, salt)
                    }

                     return Users.findByIdAndUpdate(data.reqParams, updatedPassword).then((users, error)=>{
                        users.password = ""
                        return  error ? false : true
                    })                   
                 }else{
                    return false
                 }
            }else{
                return false
            }
        })
    }
