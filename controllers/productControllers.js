const Products = require("../models/Products")

//Add products
module.exports.addProduct = async (data) =>{
    try{
        
        const productExist = await Products.findOne({name: data.product.name})
        console.log(productExist)
         // if User isAdmin
        if(data.isAdmin){

            if(productExist){
                return false// false
            }
            //Proceed to create a products
            
            let newProduct = new Products({
                name: data.product.name,
                brand: data.product.brand,
                description: data.product.description,
                price: data.product.price,
                stocks: data.product.stocks,
                imgSource: data.product.imgSource
            })

            return await newProduct.save().then((product, error)=>{
                return error ? false : true // true
            })
        
        // is not an admin
        }else{
            return true// false
        } 
    }catch(error){
        return error
    }
}

// Retrieve all products
module.exports.getAllProducts = () =>{
    return Products.find({isActive: true}).then(result => {
        return result
    })
}

// Retrieve all products
module.exports.getProductsList = (data) =>{
    return Products.find({}).then(result => {
        if(data.isAdmin){
            return result
        }else{
            return false
        }
        
    })
}


// Retrieve single products
module.exports.getSingleProduct = async (reqParams) => {
	
	const product = await Products.findById(reqParams.productId)
    const isActive = await Products.find({isActive: true}) 

    if(!product || isActive === false){
        return false
    }else{
        return product
    }
}

// Update product information

module.exports.updateProduct = async (data) =>{

try{

    if(data.isAdmin){
        // if products are not found in database
        let updateProduct={
            name: data.product.name,
            brand: data.product.brand,
            description: data.product.description,
            price: data.product.price,
            stocks: data.product.stocks,
            imgSource: data.product.imgSource
        }
        return await Products.findByIdAndUpdate(data.reqParams, updateProduct).then((course, error)=>{
            return error ? false : true
        })
    
    }else{
        return false
    }

}catch(error){
    return error
}
}

// Archive a product
module.exports.archiveProduct = async (data) =>{
try{
    if(data.isAdmin){
        let updateActiveField={
            isActive: data.product.isActive
        }

        return await Products.findByIdAndUpdate(data.reqParams, updateActiveField).then((course, error)=>{
            return error ? false : true
        })
    }else{
        return false
    }
}catch(error){
    return error
}
}