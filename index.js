const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js")


const app = express();

//MongoDB Connection
//Database Connection
mongoose.connect("mongodb+srv://admin:admin1234@cluster0.r5jlfd4.mongodb.net/Capstone2_E-commerce?retryWrites=true&w=majority",{
    useNewUrlParser: true,
    useUnifiedTopology: true
}
)

//Notifies if the connection to database is success or not
let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("Now connected to the cloud database."))


//Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.get('/', (req, res) => {
    res.send('Welcome to my website');
});
//for users endpoint
app.use("/users", userRoutes);
//for products endpoint
app.use("/products", productRoutes);

// Server listen
app.listen(process.env.PORT || 4001, () =>{
    console.log(`Server is now online on port ${process.env.PORT || 4001}`)
})